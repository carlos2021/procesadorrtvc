import cv2
def cut_frames(input,output):
    min_frames = {}
    video = cv2.VideoCapture(input)
    count = 0
    frames = 0
    bandera = 1
    while bandera:
        bandera,img = video.read()
        if count == 200:
            destine = f"{output}/{frames}.jpg"
            cv2.imwrite(destine,img)
            frames += 1
            min_frames[int((count/100)*frames)*5] = destine
            count = 0
        else:
            count += 1
    return min_frames