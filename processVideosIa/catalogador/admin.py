from atexit import register
from django.contrib import admin
from catalogador.models import *

admin.site.register(Video)
admin.site.register(Ficha)
admin.site.register(Oracion)