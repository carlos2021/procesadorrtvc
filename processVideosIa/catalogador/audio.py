import os
from dotenv import load_dotenv
from google.cloud import storage
#from google.cloud import speech
from google.cloud import speech_v1p1beta1 as speech
import site
from google.protobuf.json_format import MessageToDict
import math


storage.blob._DEFAULT_TIEMOUT = 600000000
class audio():
    def __init__(self,file):
        load_dotenv()
        self.rootPath = os.getenv("PROJECT_PATH")
        os.system(f'export GOOGLE_APPLICATION_CREDENTIALS={os.getenv("GOOGLE_APPLICATION_CREDENTIALS")}')
        self.bucketName = os.getenv("BUCKET_NAME")
        self.file = file
        self.fileName = self.file.split("/")[-1]
        self.name = file.split("/")[-1].split(".")[:-1][0]
        self.uri = f"gs://{self.bucketName}/media/{self.name}/{self.fileName}"
        

    def proces(self):
        # self.uploadFile()
        texto,oraciones = self.SpeechToText()
        os.system("unset GOOGLE_APPLICATION_CREDENTIALS")
        os.system(f"rm '{self.file}'")
        return [texto,oraciones]

    @staticmethod
    def minuteToSecs(time):
        aux = time.split(":")
        hour = int(aux[0])*3600
        minutes = int(aux[1])*60
        seconds = hour+minutes+int(aux[-1])
        return seconds
        
    def SpeechToText(self):
        transcripcion = ""
        oraciones = dict()
        client = speech.SpeechClient()
        audio = speech.RecognitionAudio(uri=self.uri)
        metadata = speech.RecognitionMetadata()
        metadata.interaction_type=(speech.RecognitionMetadata.InteractionType.PRESENTATION)
        metadata.original_media_type=(speech.RecognitionMetadata.OriginalMediaType.VIDEO)
        metadata.microphone_distance=(speech.RecognitionMetadata.MicrophoneDistance.MIDFIELD)
        metadata.recording_device_type=(speech.RecognitionMetadata.RecordingDeviceType.OTHER_OUTDOOR_DEVICE)
        metadata.recording_device_name="Cardioid Microphone"
        config = speech.RecognitionConfig(
            encoding='FLAC',
            enable_automatic_punctuation = True,
            enable_word_time_offsets = True,
            enable_word_confidence = True,
            enable_spoken_punctuation = True,
            use_enhanced=True,
            language_code = "es-CO",
            alternative_language_codes=["es-PE","es-US","es-MX","es-EC","es-PA","es-VE",
            "es-PR","es-CR","es-AR","es-BO","es-CL","es-DO","es-ES","es-UY"
            ],
            metadata = metadata,
            speech_contexts = [
                {"phrases":[
                "el derogado","es un","buscando","Crypton","subersivo","narcotrafico","FARC","ELN","proceso"
                "atscrito","DAS","ni tratos","Escobar","vinculado","preliminar","sumario","compete","ministro de","unos","en la"
                ],
                "boost":20
                },
                {
                    "phrases":["preside","en cabeza de ","eso va","temas","vuelva","albañil","albañileria"],
                    "boost":18
                },
                {
                    "phrases":["Esteban","Andres","Escobar","David Gallon Henao","Arquidio Herrera","Santiago","turbay","pastrana","congreso",
                    "senado"
                    ],
                    "boost":19
                },
                {"phrases":["el haber",
                "casi que","segun el caso","cuente con",
                "contraste","nos esta","que hacer","esas",
                "politíca","la justicia","es una",
                "don","disque un","cualesquier cosa","ella","en el","extorsiones","he sido","hospede","se daño esto","ni",
                "version","mirare","a mi","por lo menos","encabeza","escogidos","han avanzado","vicepresidencia"
                ],
                "boost":15
                },
                {
                    "phrases":["grabar","grabando","graba","en lo que","ya tenemos","y que","que debe"],
                    "boost":13
                },
                {"phrases":["encontrarse","alcalde","sucidios","asesinato"
                        "juez","que desplego","condena","fiscalia","legal","los","falsa"
                        ,"esta viva","allegemos","no es","los he","la niña mia","nosotros","hospital","no me identifico",
                        "que se","a usted","es que aqui","esa es","consejos",
                ],
                "boost":10
                },
                {"phrases":["$DAY","$YEAR","$MONTH","$MONEY","%OPERAND","$PERCENT","$OOV_CLASS_DIGIT_SEQUENCE"
                
                ],
                "boost":5
                }
            ],
        )
        response = client.long_running_recognize(config=config, audio=audio)
        data = response.result(timeout=600000)
        for b in data.results:
            transcripcion = transcripcion + b.alternatives[0].transcript
            oracion = b.alternatives[0].transcript
            aux = [(self.minuteToSecs(str(w.start_time).split(".")[0]),self.minuteToSecs(str(w.end_time).split(".")[0])) for w in b.alternatives[0].words]
            aux = sorted(aux)
            start = math.floor(aux[0][0]*0.75)
            end = math.floor(aux[-1][1]*0.75)
            intervalo = f"{start}-{end}"
            oraciones[intervalo]=oracion
        return [transcripcion,oraciones]

    # def uploadFile(self):
    #     cliente = storage.Client()
    #     bucket  = cliente.bucket(self.bucketName)
    #     fileName = self.file.split("/")[-1]
    #     #print(self.file)
    #     blob = bucket.blob(fileName)
    #     blob.upload_from_filename(self.file)
    #     self.uri = f"gs://{self.bucketName}/{fileName}"
        
