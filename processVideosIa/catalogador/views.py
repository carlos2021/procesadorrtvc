import os
from pickle import STOP
import site
from django.http import HttpResponse, JsonResponse
from dotenv import load_dotenv
from datetime import datetime
from catalogador.audio import audio
from catalogador.video import cut_frames
from catalogador.bussines import *
import subprocess
import json
load_dotenv()


noise = ["Don"]
rutaArchivos = os.getenv("PROCES_FOLDER")


def peso(fname):
    sizeInt = (os.stat(fname).st_size/1000000)
    size = str(int(sizeInt))
    return size + " megabytes."


def preprocesarVideo(archivo):
    nombre = archivo.replace(' ','-')
    archivoOriginal = f"{rutaArchivos}/{archivo}"
    os.mkdir(f"{os.getenv('PROJECT_PATH')}/processVideosIa/temporaryMedia/{archivo.replace(' ','-').split('.')[0]}")
    os.mkdir(f"{os.getenv('PROJECT_PATH')}/processVideosIa/temporaryMedia/{archivo.replace(' ','-').split('.')[0]}/fotogramas")
    archivoOptimizado = f"{os.getenv('PROJECT_PATH')}/processVideosIa/temporaryMedia/{archivo.replace(' ','-').split('.')[0]}/{archivo.replace(' ','-')}"
    evento = subprocess.Popen(["ffmpeg","-nostdin","-i",archivoOriginal,"-b:v","150k","-bufsize","150k","-r","20","-s","854x480","-vcodec","libx264","-threads","6","-aspect","854:480",archivoOptimizado,"-loglevel","fatal"])
    evento.communicate()
    createFolderBucket(archivoOptimizado)
    createFolderBucket(archivoOptimizado, "fotogramas")
    uploadFileBucket(archivoOptimizado)
    cuantoPesa = peso(archivoOriginal)
    insertFileFistTime(nombre,archivo, archivoOptimizado, cuantoPesa,archivoOriginal)
    return True


def prepocesarDesdeCarpeta(request):
    archivos = os.listdir(rutaArchivos)
    for archivo in archivos:
        print("entro")
        preprocesarVideo(archivo)
    return JsonResponse({"message":"completado"})


#--------------------------------------------
#------------------------------------------------
#--------------------------------------------
def segundo(archivoOptimizado, archivo):
    relacion_min = cut_frames(archivoOptimizado,f"{os.getenv('PROJECT_PATH')}/processVideosIa/temporaryMedia/{archivo.replace(' ','-').split('.')[0]}/fotogramas")
    insert_frames(relacion_min, archivoOptimizado.split("/")[-1])
    return True


def divirFotogramas(request):
    que = cualesProcesar()
    print(que)
    for a in que:
        segundo(a.rutaArchivoOptimizado, a.archivo)
    return JsonResponse({"message":"completado"})


#--------------------------------------------
#------------------------------------------------
#--------------------------------------------
def tercero(archivoOptimizado):
    videoToAudio(archivoOptimizado)
    return True


def crearFlac(request):
    que = cualesProcesar()
    for a in que:
        tercero(a.rutaArchivoOptimizado)
    return JsonResponse({"message":"completado"})


#--------------------------------------------
#------------------------------------------------
#--------------------------------------------
def cuarto(archivoOptimizado,archivo):
    aux = archivoOptimizado.split("/")[:-1]
    ruta = "/".join(aux)
    name = archivoOptimizado.split("/")[-1].split(".")[:-1][0]
    rutaFlac = f"{ruta}/{name}.flac"
    audioController = audio(rutaFlac)
    transcripcion,oraciones = audioController.proces()
    insertFileSecondTime(archivo,transcripcion, oraciones) 
    return True


def extraerTranscripcionOraciones(request):
    que = cualesProcesar()
    for a in que:
        cuarto(a.rutaArchivoOptimizado, a.archivo)
    return JsonResponse({"message":"completado"})

def index(request):
    if request.method == 'GET':
        return HttpResponse("todo esta bien..!")