from decimal import DefaultContext
from distutils import extension
from pyexpat import model
from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver

class Video(models.Model):
    nombre = models.CharField(max_length=100,unique=True,  verbose_name="Fichas")
    transcripcion = models.TextField()
    archivo = models.CharField(max_length=300,null=False)
    procesado = models.BooleanField(default=False)
    ocr = models.TextField(blank=True)
    tipo = models.CharField(max_length=100, blank= True,null=True)
    rutaOriginal = models.CharField(max_length=150,blank= True,null=True)
    rutaArchivoOptimizado = models.CharField(max_length=150,blank= True,null=True)
    tieneIa = models.BooleanField(default=False)   
    preProcesado = models.BooleanField(default=False)
    
    # def __str__(self):
    #     return self.nombre
    

class Ficha(models.Model):
    video = models.OneToOneField(Video, on_delete=models.CASCADE)
    encabezamiento = models.CharField(max_length=100,blank=True,null=True)#                         000
    horaCampoCinco = models.CharField(max_length=300,blank= True,null=True)#                        005
    caracteristicaseis = models.CharField(max_length=300,blank= True,null=True)#                    006
    descripcionFisicaFijo = models.CharField(max_length=300,blank= True,null=True)#                 007
    longitudFija = models.TextField(blank=True,null=True)#                                          008
    centroCatalogador=models.CharField(default="RTVC-FPFC", max_length=100, 
                                        blank=True, null=True)#                                     040-A -- fuenteCatalogacion
    lenguaCatalogacion=models.CharField(default="spa", max_length=100, 
                                        blank=True, null=True)#                                     040-B -- fuenteCatalogacion
    codigoLenguaTexto=models.CharField(default="spa", max_length=100, 
                                        blank=True, null=True)#                                     041-A
    numeroClasificacion = models.CharField(max_length=300,blank= True,null=True)#                   084

    extension = models.CharField(max_length=300,blank= True,null=True)#                             300-A -- descripcionFisica
    caracteristicasFisicas = models.CharField(max_length=300,blank= True,null=True)#                300-B
    dimensiones = models.CharField(max_length=300,blank= True,null=True)#                           300-C ----- Peso
    materialAcompanante = models.CharField(max_length=300,blank= True,null=True)#                   300-E
    duracion = models.CharField(max_length=300,blank = True,null=True)#                             306
    terminoTipoContenido = models.CharField(max_length=300,blank= True,null=True,
                                            default="gráfico proyectado")#                          336-A -- tipoDeContenido
    codigoTipoContenido = models.CharField(max_length=300,blank= True,null=True,
                                            default="tdi")#                                         336-B
    fuenteTipoContenido = models.CharField(max_length=300,blank= True,null=True,
                                            default="rdacontenido")#                                336-2
    nombreTipoMedio = models.CharField(max_length=300,blank= True,null=True,
                                        default="videograbacón")#                                   337-A -- tipoDeMedio
    codigoTipoMedio = models.CharField(max_length=300,blank= True,null=True,
                                        default="v")#                                               337-B
    fuenteTipoMedio = models.CharField(max_length=300,blank= True,null=True,
                                        default="rdamedio")#                                        337-2
    nombreTipoSoporte = models.CharField(max_length=300,blank= True,null=True,
                                        default="recurso en linea")#                                338-A -- tipoDeSoporte
    codigoTipoSoporte = models.CharField(max_length=300,blank= True,null=True,
                                        default="cr")#                                              338-B
    fuenteTipoSoporte = models.CharField(max_length=300,blank= True,null=True,
                                        default="rdaSoporte")#                                      338-2

    restriccionesLegales = models.CharField(default="Sin especificar", 
                                            max_length=300,blank= True,null=True)#                  506-A -- RestriccionDeAcceso
    terminologiaNormalizadaRestricciones = models.CharField(default="Acceso online con previa autorización",
                                                 max_length=300,blank= True,null=True)#             506-F
    terminologiaNormalizada = models.CharField(max_length=300,blank= True,null=True,
                                                default="Catalogación básica")#                     583-A -- notaDeAccion
    fechaAccion = models.CharField(max_length=300,blank= True,null=True)#                           583-C
    mediosRealizaAccion = models.CharField(max_length=300,blank= True,null=True,
                                            default="Por medio de la herramienta de IA, se realiza el registro mínimo de metadatos de identificación y contenido del documento en el software bibliográfico Koha, estructurado bajo el formato MARC21, utilizando las normas AACR2, IASA, RDA y en concordancia con el manual interno de catalogación de Señal Memoria, establecido por el área de Gestión de Colecciones.")# 583-I
    codigoCatalogador = models.CharField(max_length=300,blank= True,null=True,
                                        default="delphi_user")#                                     583-K
    estadoMaterial = models.CharField(max_length=300,blank= True,null=True)#                        583-L

    nombreEntidad810 = models.CharField(max_length=300,blank= True,null=True)#                      810
    localizacion = models.CharField(max_length=300,blank= True,null=True)#                          852-A -- etiqueta(Localizacion)
    parteClasificacion = models.CharField(max_length=300,blank= True,null=True)#                    852-H
    prefijoSignatura = models.CharField(max_length=300,blank= True,null=True)#                      852-K
    designacionUnidadFisica = models.CharField(max_length=300,blank= True,null=True)#               852-P
    codec = models.CharField(max_length=300,blank= True,null=True, default="video/mp4")#                                 856-Q -- etiqueta(RecursoEnLinea)
    host = models.CharField(max_length=300,blank= True,null=True)#                                  856-A
    url = models.CharField(default="No presenta", max_length=300,blank= True,null=True)#            856-U
    textoEnlace = models.CharField(max_length=300,blank= True, null=True)#                           856-Y

    fuenteSistemaClasificacion = models.CharField(max_length=300,blank= True,null=True, default="o")#            942-2 -- elementosPuntoAcceso
    tipoItemKoha = models.CharField(default="Archivo Audiovisual" , max_length=300,blank= True,null=True)#                          942-C
    autorizacion = models.CharField(default="1", max_length=300,blank= True,null=True)#             942-N

    tipoAoV = models.CharField(max_length=300,blank= True,null=True)
    
    datecreated = models.CharField(max_length=300,blank= True,null=True) #biblio
    illus = models.CharField(max_length=300,blank= True,null=True) #biblio
    pages = models.CharField(max_length=300,blank= True,null=True) #biblio
    size = models.CharField(max_length=300,blank= True,null=True) # biblio

    # def __str__(self):
    #     return self.video

class SubFicha(models.Model):
    ficha = models.ForeignKey(Ficha, on_delete=models.CASCADE)
    biblionumber = models.CharField(max_length=300,blank= True,null=True)
    tiempoInicial = models.IntegerField(blank=True,null=True, default=0)
    tiempoFinal = models.IntegerField(blank=True,null=True)
    textoContenido = models.TextField(blank=True,null=True)

    numeroClasificacionOpcional = models.CharField(max_length=300,blank= True,null=True)#           090

    nombrePersona = models.CharField(max_length=300,blank= True,null=True)#                         100-A -- autorPesona
    fechasAsociadasNombre  = models.CharField(max_length=300,blank= True,null=True)#                100-D 
    terminoIndicativoFuncion = models.CharField(max_length=300,blank= True,null=True)#              100-E 
    titulosTerminosAsociados = models.CharField(max_length=300,blank= True,null=True)#              100-C  
    nombreEntidad = models.CharField(max_length=300,blank= True,null=True)#                         110-A -- autorEntidad
    funcion = models.CharField(max_length=300,blank= True,null=True)#                               110-E
    unidadSubordinada = models.CharField(max_length=300,blank= True,null=True)#                     110-B
    numeroCongreso = models.CharField(max_length=300,blank= True,null=True)#                        111-N -- autorEvento
    nombreReunion = models.CharField(max_length=300,blank= True,null=True)#                         111-A
    fechaReunion = models.CharField(max_length=300,blank= True,null=True)#                          111-D
    lugarReunion = models.CharField(max_length=300,blank= True,null=True)#                          111-C

    tituloUniforme = models.CharField(max_length=300,blank= True,null=True)#                        243-A -- tituloUniformeColectivo
    fechaTituloUniforme = models.CharField(max_length=300,blank= True,null=True)#                   243-F
    title = models.CharField(max_length=300,blank= True,null=True)#                                 245-A -- titulo
    fechaTitulo = models.CharField(max_length=300,blank= True,null=True)#                           2A45-F
    mencionResponsabilidad = models.CharField(max_length=300,blank= True,null=True)#                245-C
    restoTitulo = models.CharField(max_length=300,blank= True,null=True)#                           245-B
    lugarProduccion = models.CharField(max_length=300,blank= True,null=True)#                       264-A -- produccion
    entidadProductora = models.CharField(max_length=300,blank= True,null=True)#                     264-B
    anoProduccion = models.CharField(max_length=300,blank= True,null=True)#                         264-C

    coleccion = models.CharField(max_length=300,blank= True,null=True)#                             490

    notaGeneral = models.CharField(max_length=300,blank= True,null=True)#                           500-A -- notaGeneral
    especificacionMateriales = models.CharField(max_length=300,blank= True,null=True)#              500-3
    notaContenido = models.TextField(default=None,blank= True,null=True)#                           505
    notaCreditos = models.CharField(max_length=300,blank= True,null=True)#                          508
    notaElenco = models.CharField(max_length=300,blank= True,null=True)#                            511

    fuenteEncabezamiento = models.CharField(default="Tesauro de Señal Memoria, RTVC", 
                                            max_length=300, blank= True,null=True)#                 600-2 -- Tema(Persona)
    nombrePersonaTema = models.CharField(max_length=300,blank= True,null=True)#                     600-A
    fechasAsociadasNombreTema = models.CharField(max_length=300,blank= True,null=True)#            600-D
    funcionTema = models.CharField(max_length=300,blank= True,null=True)#                           600-E
    periodoMandato = models.CharField(max_length=300,blank= True,null=True)#                        600-Y
    nombreEntidadTema = models.CharField(max_length=300,blank= True,null=True)#                     610 -- Tema(Entidad)
    fuenteEncabezamiento611 = models.CharField(max_length=300, blank= True,null=True)#              611-2 --Tema(Evento)
    numeroCongreso611 = models.CharField(max_length=300,blank= True,null=True)#                     611-N
    nombreEventoTema = models.CharField(max_length=300,blank= True,null=True)#                      611-A
    fechaCongreso = models.CharField(max_length=300,blank= True,null=True)#                        611-D
    sedeCongreso = models.CharField(max_length=300,blank= True,null=True)#                          611-C
    fuenteEncabezamiento650  = models.CharField(max_length=300,blank= True,null=True)#              650-2 -- temas(Normalizados)
    tema = models.CharField(max_length=300,blank= True,null=True)#                                  650-A
    subdivisionGeneral = models.CharField(max_length=300,blank= True,null=True)#                    650-X
    palabraClave = models.CharField(max_length=300,blank= True,null=True)#                          653
    fuenteTermino = models.CharField(max_length=300,blank= True,null=True, default="Tesauro de Señal Memoria, RTVC")#                         655-2 -- generoYforma
    genero = models.CharField(max_length=300,blank= True,null=True)#                                655-A
    forma = models.CharField(max_length=300,blank= True,null=True)#                                 655-V

    # def __str__(self):
    #     return self.biblionumber

class TipoMetadato(models.Model):
    nombre = models.CharField(max_length=100)

    def __str__(self):
        return self.nombre



class ReconocedorVoz(models.Model):
    ficha = models.ForeignKey(Ficha,on_delete=models.CASCADE)
    personajeHablante = models.CharField(max_length=200,unique=True)
    minutoInicial = models.IntegerField(blank=True,null=True)
    minutoFinal = models.IntegerField(blank=True,null=True)
    def __str__(self):
        return self.personajeHablante


class Metadato(models.Model):
    tipo = models.ForeignKey(TipoMetadato,on_delete=models.CASCADE)
    nombre = models.CharField(max_length=200,unique=True)
    url = models.CharField(max_length=300,blank=True)
    descripcion = models.TextField(blank=True)

    def __str__(self):
        return self.nombre

class MetadatoFicha(models.Model):
    metadato = models.ForeignKey(Metadato,on_delete=models.CASCADE)
    ficha = models.ForeignKey(Ficha,on_delete=models.CASCADE)
    minuto = models.IntegerField(blank=True,null=True)

class Oracion(models.Model):
    inicio = models.IntegerField()
    fin = models.IntegerField()
    texto = models.TextField()
    ficha = models.ForeignKey(Ficha,on_delete=models.CASCADE)

class Transcriptor(models.Model):
    types =  (
        ("Rtvc","Rtvc"),
        ("Da","Delphi")
    )
    usuario = models.OneToOneField(User,on_delete=models.CASCADE)
    tipo = models.CharField(max_length=5,choices=types, default="Rtvc")

    def __str__(self):
        return self.usuario.username

@receiver(post_save, sender=User)
def crear_usuario_perfil(sender, instance, created, **kwargs):
    if created:
        Transcriptor.objects.create(usuario=instance)

@receiver(post_save, sender=User)
def guardar_usuario_perfil(sender, instance, **kwargs):
    instance.transcriptor.save()

class Registro(models.Model):
    ficha = models.ForeignKey(Ficha,on_delete=models.CASCADE)
    transcriptor = models.ForeignKey(Transcriptor,on_delete=models.CASCADE)
    fecha =  models.DateTimeField(auto_now_add=True)

class Fotograma(models.Model):
    ficha = models.ForeignKey(Ficha,on_delete=models.CASCADE)
    segundo = models.IntegerField()
    archivo = models.CharField(max_length=300,null=False)
    resumen = models.CharField(max_length=300,blank= True, null=True)
    fecha = models.DateField(max_length=300,blank= True, null=True)
    personaje = models.CharField(max_length=300,blank= True, null=True)
    descripcion = models.CharField(max_length=300,blank= True, null=True)
    disponible = models.BooleanField(default=False)
    divisor = models.IntegerField(default=0)
