import os
from re import A
import site
from dotenv import load_dotenv
from google.cloud import storage
import sys
import django

load_dotenv()
site.addsitedir(os.getenv("PROJECT_PATH"))
sys.path.extend([f'{os.getenv("PROJECT_PATH")}/Backend/api',])
os.environ.setdefault("DJANGO_SETTINGS_MODULE","api.settings")
import subprocess
django.setup()
from catalogador import models as model

    
def insertFileSecondTime(archivo,transcripcion,oraciones): 
    videoParaActualizar =model.Video.objects.filter(archivo=archivo)[0]
    videoParaActualizar.transcripcion = transcripcion
    videoParaActualizar.preProcesado = True
    videoParaActualizar.save()
    ficha = model.Ficha.objects.filter(video=videoParaActualizar)[0]
    for oracion in oraciones.keys():
        inicio = int(oracion.split("-")[0])
        fin = int(oracion.split("-")[1])
        texto = "".join(oraciones[oracion])
        oracionBd = model.Oracion(inicio=inicio,fin=fin,texto=texto,ficha= ficha)
        oracionBd.save()
    return archivo

 
def insertFileFistTime(nombre,archivo, rutaArchivoOptimizado, cuantoPesa, archivoOriginal):
    video = model.Video(nombre = nombre,archivo = archivo.replace(' ','-'),  \
                        rutaArchivoOptimizado= rutaArchivoOptimizado, \
                        rutaOriginal = archivoOriginal)
    video.save()
    ficha = model.Ficha(video = video)
    ficha.save()
    subFicha = model.SubFicha(ficha = ficha)
    subFicha.save()
    
def cualesProcesar():
    video = model.Video.objects.filter(preProcesado = False )
    return video


def videoToAudio(file):
    '''Objetivo: convertir un archivo de video a un archivo de audio.flac 
    Entrada: ruta a un archivo de video .mp4
    Proceso: se convierte el archivo y se escribe en una direccionada
    salida: ruta al audio '''
    aux = file.split("/")[:-1]
    ruta = "/".join(aux)
    name = file.split("/")[-1].split(".")[:-1][0]
    a = subprocess.Popen(["ffmpeg","-nostdin","-i",f"{file}","-c:a" ,"flac", "-compression_level", "12" ,"-ac","1","-threads","6","-filter:a",'atempo=0.75',f"{ruta}/{name}.flac","-loglevel","fatal"])
    a.communicate()
    rutaFlac = f"{ruta}/{name}.flac"
    uploadFileBucket(rutaFlac)
    return True    

    
def normalizarMinutos(minutos):
    try:
        minutosRecortar = minutos.split(";")
        minutosRecortar = [int(float(a)) for a in minutosRecortar]
    except:
        minutosRecortar = [int(float(minutos))]
    return list(set(minutosRecortar))


def getMetadata(idficha):
    salida = {
        "personaje":[],
        "lugar":[],
        "actor":[],
        "titulo":[]
    }
    meta_min = []
    metadatos = model.MetadatoFicha.objects.filter(ficha=idficha)
    for metadato in metadatos:
        aux = {
            "minuto":metadato.minuto,
            "descripcion":metadato.metadato.descripcion,
            "metadato":metadato.metadato.nombre,
            "url":metadato.metadato.url
        }
        meta_min.append(aux)
        tipo = metadato.metadato.tipo.nombre
        m = metadato.metadato.nombre
        try:
            if(m not in salida[tipo]):
                salida[tipo].append(m)
        except:
            continue
    return [salida,meta_min]


def insert_frames(frames,archivo):
    video = model.Video.objects.filter(nombre=archivo)[0]
    ficha = model.Ficha.objects.filter(video=video)[0]
    for f in frames.keys():
        url=uploadFileBucket(frames[f], archivo)
        # aux_archivo = frames[f].split("/")[-3:]
        # aux_archivo = "/".join(aux_archivo)
        frame = model.Fotograma(ficha=ficha,segundo=f,archivo=url)
        frame.save()
        

def eliminar_meta(video,meta):
    meta = model.Metadato.objects.filter(nombre=meta)
    ficha = model.Ficha.objects.filter(archivo=video)
    model.MetadatoFicha.objects.filter(metadato=meta[0],ficha=ficha[0]).delete()


def createFolderBucket(pathFile, fotogramas=None):
    os.system(f'export GOOGLE_APPLICATION_CREDENTIALS={os.getenv("GOOGLE_APPLICATION_CREDENTIALS")}')
    bucketName = os.getenv("BUCKET_NAME")
    cliente = storage.Client()
    bucket  = cliente.bucket(bucketName)
    fileName = pathFile.split("/")[-1].split(".")[0]
    if fotogramas == None:
        blob = bucket.blob(f"media/{fileName}/")
        blob.upload_from_string(f"newFolder/")
    else:
        blob = bucket.blob(f"media/{fileName}/{fotogramas}/")
        blob.upload_from_string(f"newFolder/")
    uri = f"gs://{bucketName}/media/{fileName}/"
    os.system("unset GOOGLE_APPLICATION_CREDENTIALS")
    # print(uri)


def uploadFileBucket(pathfile, archivo=None):
    os.system(f'export GOOGLE_APPLICATION_CREDENTIALS={os.getenv("GOOGLE_APPLICATION_CREDENTIALS")}')
    bucketName = os.getenv("BUCKET_NAME")
    cliente = storage.Client()
    bucket  = cliente.bucket(bucketName)
    fileName = pathfile.split("/")[-1]
    file = fileName.split(".")[0]
    #print(self.file)
    if archivo == None: 
        blob = bucket.blob(f"media/{file}/{fileName}")
        blob.upload_from_filename(pathfile)
        blob.make_public()
    else:
        nameArchivo = archivo.split(".")[0]
        blob = bucket.blob(f"media/{nameArchivo}/fotogramas/{fileName}")
        blob.upload_from_filename(pathfile)
        blob.make_public()
    url = blob.public_url
    os.system("unset GOOGLE_APPLICATION_CREDENTIALS")
    return url